// ex1
document.getElementById('TinhThue').onclick=function(){
    // input khai báo họ tên, thu nhập năm, số người phụ thuộc
    var HoTen = DomId('HoTen')
    var ThuNhap = Number(DomId('ThuNhap'))
    var SoNguoi = Number(DomId('SoNguoi'))
    // output tiền thuế
    var TienThue=0
    // progress: tính thu nhập chịu thuế, tính thuế tùy theo %thu nhập
    var ThuNhapChiuThue=ThuNhap-4e+6-SoNguoi*1.6e+6

    if (ThuNhapChiuThue>0) {
        if (ThuNhapChiuThue<=60e+6) {TienThue=ThuNhapChiuThue*0.05}
        else if (ThuNhapChiuThue<=120e+6) {TienThue=ThuNhapChiuThue*0.1}
        else if (ThuNhapChiuThue<=210e+6) {TienThue=ThuNhapChiuThue*0.15}
        else if (ThuNhapChiuThue<=384e+6) {TienThue=ThuNhapChiuThue*0.2}
        else if (ThuNhapChiuThue<=624e+6) {TienThue=ThuNhapChiuThue*0.25}
        else if (ThuNhapChiuThue<=960e+6) {TienThue=ThuNhapChiuThue*0.3}
        else if (ThuNhapChiuThue>960e+6) {TienThue=ThuNhapChiuThue*0.35}
    }
    else {alert("Số tiền thu nhập không hợp lệ")}

    document.getElementById('TienThue').innerHTML=`Họ tên: ${HoTen}; Tiền thuế thu nhập cá nhân: ${Intl.NumberFormat().format(TienThue)} VND`
}

// ex2
function ChonDoanhNghiep(){ /* hiển thị số kết nối khi chọn doanh nghiệp */
    var LoaiKH=document.getElementById('LoaiKH')
    var LoaiKHchon = LoaiKH.options[LoaiKH.selectedIndex].value;    
    if (LoaiKHchon=='DoanhNghiep')
    {document.getElementById('SoKN').type="number"}
    else {document.getElementById('SoKN').type="hidden"}
    return LoaiKHchon
}

document.getElementById('Tinhtiencap').onclick=function(){
    // input: Loại khách hàng, mã khách hàng, số kênh, số kết nối
    var MaKH = DomId('MaKH')
    var SoKenh = Number(DomId('SoKenh'))
    var SoKN = Number(DomId('SoKN'))
    LoaiKHchon=ChonDoanhNghiep()
    // output tiền cáp
    TienCap=0
    // progress: tính tiền cáp dựa theo công thức
    if (LoaiKHchon=='NhaDan'){
        TienCap=4.5+20.5+7.5*SoKenh
    }
    else if (LoaiKHchon=='DoanhNghiep'){
        if (SoKN<=10){
            TienCap=15+75+50*SoKenh
        }
        else {TienCap=15+75+(SoKN-10)*5+50*SoKenh
        }
    }
    else {alert('Hãy chọn loại khách hàng')}
    document.getElementById('TienCap').innerHTML=`Mã khách hàng: ${MaKH}; Tiền cáp: $${TienCap.toLocaleString('en',{minimumFractionDigits:2, maximumFractionDigits:2})}`
}

function DomId(Id) {
    return document.getElementById(Id).value
}
